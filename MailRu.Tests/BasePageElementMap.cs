﻿using OpenQA.Selenium;

namespace MailRu {

    public class BasePageElementMap {

        protected readonly IWebDriver browser;

        public BasePageElementMap() {
            browser = Driver.Browser;
        }
    }
}
