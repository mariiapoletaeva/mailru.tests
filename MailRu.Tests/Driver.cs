﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace MailRu {

    public static class Driver {

        public enum BrowserTypes {
            Chrome
        }

        private static WebDriverWait browserWait;
        private static IWebDriver browser;

        public static IWebDriver Browser {
            get {
                if (browser == null) {
                    throw new InvalidOperationException("The WebDriver browser instance was not initialized.");
                }
                return browser;
            }
            private set {
                browser = value;
            }
        }

        public static WebDriverWait BrowserWait {
            get {
                if (browserWait == null || browser == null) {
                    throw new NullReferenceException("The WebDriver browser wait instance was not initialized.");
                }
                return browserWait;
            }
            private set {
                browserWait = value;
            }
        }

        public static void StartBrowser(BrowserTypes browserType = BrowserTypes.Chrome, int defaultTimeOut = 30) {
            switch (browserType) {
                case BrowserTypes.Chrome:
                    Browser = new ChromeDriver();
                    Browser.Manage().Window.Maximize();
                    break;
            }
            BrowserWait = new WebDriverWait(Driver.Browser, TimeSpan.FromSeconds(defaultTimeOut));
        }

        public static void StopBrowser() {
            Browser.Quit();
            Browser = null;
            BrowserWait = null;
        }

        public static void WaitElement(By elementLocator) {
            WebDriverWait waitForElement = new WebDriverWait(browser, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(elementLocator));
        }

        public static void SwitchToFrame(By elementLocator) {
            browser.SwitchTo().Frame(browser.FindElement(elementLocator));
        }

        public static void SwitchToDefaultContent() {
            browser.SwitchTo().DefaultContent();
        }
    }
}

