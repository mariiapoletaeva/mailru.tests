﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace MailRu {

    [TestClass]
    public class MailTests {

        private const string Url = "https://mail.ru";
        private const string Login = // fill your login
        private const string Password = // fill your password
        private const string Addressee = "mrpltv.cfg@gmail.com";

        [TestInitialize]
        public void SetupTest() {
            Driver.StartBrowser();
        }

        [TestCleanup]
        public void TeardownTest() {
            Driver.StopBrowser();
        }

        [TestMethod]
        public void SendMail() {
            Driver.Browser.Navigate().GoToUrl(Url);

            var pageMap = new MailPageElementMap();

            pageMap.LoginBox.SendKeys(Login);
            pageMap.PasswordBox.SendKeys(Password);
            pageMap.GoButton.Click();

            Driver.WaitElement(By.ClassName("b-toolbar__item"));

            pageMap.CreateLetterButton.Click();

            Driver.WaitElement(By.XPath(".//textarea[@data-original-name=\"To\"]"));
            pageMap.ToBox.SendKeys(Addressee);
            pageMap.SubjectBox.SendKeys("Test");

            Driver.SwitchToFrame(By.XPath(".//iframe[@title=\"{#aria.rich_text_area}\"]"));
            pageMap.MailBody.SendKeys("Hi! I'm Test.");
            Driver.SwitchToDefaultContent();

            pageMap.SendButton.Click();

            Driver.WaitElement(By.Id("b-compose__sent"));
        }
    }
}
