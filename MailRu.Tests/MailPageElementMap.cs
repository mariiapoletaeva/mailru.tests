﻿using OpenQA.Selenium;

namespace MailRu {

    public class MailPageElementMap : BasePageElementMap {

        public IWebElement LoginBox {
            get {
                return browser.FindElement(By.Id("mailbox:login"));
            }
        }

        public IWebElement PasswordBox {
            get {
                return browser.FindElement(By.Id("mailbox:password"));
            }
        }

        public IWebElement GoButton {
            get {
                return browser.FindElement(By.Id("mailbox:submit"));
            }
        }

        public IWebElement CreateLetterButton {
            get {
                return browser.FindElement(By.XPath(".//a[@data-name=\"compose\"]"));
            }
        }

        public IWebElement ToBox {
            get {
                return browser.FindElement(By.XPath(".//textarea[@data-original-name=\"To\"]"));
            }
        }

        public IWebElement SubjectBox {
            get {
                return browser.FindElement(By.Name("Subject"));
            }
        }

        public IWebElement MailBody {
            get {
                return browser.FindElement(By.Id("tinymce"));
            }
        }

        public IWebElement SendButton {
            get {
                return browser.FindElement(By.XPath(".//div[@data-name=\"send\"]"));
            }
        }

        public IWebElement SendMailResult {
            get {
                return browser.FindElement(By.Id("b-compose__sent"));
            }
        }
    }
}